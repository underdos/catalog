package net.kusnadi.catalog.catalog.controller;

import net.kusnadi.catalog.catalog.entity.Product;
import net.kusnadi.catalog.catalog.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public Page<Product> findProducts(Pageable page) {
        return productRepository.findAll(page);
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
    public Page<Product> findProductsById(@PathVariable("id") Pageable page) {
        return productRepository.findAll(page);
    }

}
