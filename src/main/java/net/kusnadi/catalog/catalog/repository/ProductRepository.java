package net.kusnadi.catalog.catalog.repository;

import net.kusnadi.catalog.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductRepository extends PagingAndSortingRepository<Product, String> {
}
